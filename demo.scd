s.quit;
s.options.memSize = 65536 * 8;
s.reboot;

MIDIClient.init;
MIDIIn.connectAll;

// A SynthDef with ADSR envelope
SynthDef("demo", {
	arg freq = 440, amp = 0.1, gate = 1, mod = 1750, bend=0;
	var sound, env;
	env = Env.adsr(0.05, 0.035, 0.5, 0.95).kr(4, gate);
	sound = SinOsc.ar(freq * bend.midiratio, env);
	sound = RLPF.ar(sound, mod, 1);
	sound = sound * env;
	Out.ar(0, sound)
}).add;

// verify that the Synth "demo" works
~test = Synth.new(\demo, []);

// the pitch wheel MIDI values range from 0-16383, and 8192 means no bend, so we set ~bend to 8192
~bend = 8192;

// the modulation wheel MIDI values range from 0-127, so we set ~mod to 62 
~mod = 63;

(
var noteArray = Array.newClear(128); // array has one index per possible MIDI note

MIDIdef.noteOn(\myKeyDown, {
	arg vel, note, bend;
	noteArray[note] = Synth("demo", [
		\freq, note.midicps, 
		\amp, vel.linlin(0, 127, 0, 0.7), 
		\bend, ~bend.linlin(0, 16383, -2, 2), 
		\mod, ~mod.linlin(0, 127, 400, 2400)
	]);
	["NOTE ON", note].postln;
});

MIDIdef.noteOff(\myKeyUp, {
	arg vel, note;
	/*noteArray[note].set(\gate, 0);*/
	noteArray[note].release(3);
	["NOTE OFF", note].postln;
});

MIDIdef.bend(\bendTest, {
	arg val, chan, src;
	~bend = val;
	noteArray.do{
		arg note; 
		note.set(\bend, val.linlin(0, 16383, -2, 2))
	};
	[val, chan, src].postln;
});

MIDIdef.cc(\mod, {
	arg value, chan, src;
	~mod = value;
	noteArray.do{
		arg note; 
		note.set(\mod, value.linlin(0, 127, 400, 2400))
	};
	[value, chan, src].postln;
}, 1);

s.scope;
